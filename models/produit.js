'use strict';
module.exports = (sequelize, DataTypes) => {
  const Produit = sequelize.define('Produit', {
    nom: DataTypes.STRING,
    prix: DataTypes.INTEGER,
    description: DataTypes.STRING,
    img: DataTypes.STRING
  }, {});
  Produit.associate = function(models) {
    // associations can be defined here
  };
  return Produit;
};