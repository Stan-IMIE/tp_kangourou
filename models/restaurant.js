'use strict';
module.exports = (sequelize, DataTypes) => {
  const Restaurant = sequelize.define('Restaurant', {
    nom: DataTypes.STRING,
    adresse: DataTypes.STRING,
    tel: DataTypes.STRING,
    description: DataTypes.STRING,
    img: DataTypes.STRING
  }, {});
  Restaurant.associate = function(models) {
    // associations can be defined here
  };
  return Restaurant;
};