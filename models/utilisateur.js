// 'use strict';
// module.exports = (sequelize, DataTypes) => {
//   const Utilisateurs = sequelize.define('Utilisateurs', {
//     nom: DataTypes.STRING,
//     prenom: DataTypes.STRING,
//     tel: DataTypes.STRING,
//     mail: DataTypes.STRING,
//     mdp: DataTypes.STRING
//   }, {});
//   Utilisateurs.associate = function(models) {
//     // associations can be defined here
//   };
//   return Utilisateurs;
// };
//


'use strict';
const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {

  const Utilisateurs = sequelize.define('Utilisateurs', {
    nom:{
      type: DataTypes.STRING
    },
    prenom:{
      type: DataTypes.STRING
    },
    tel: {
      type: DataTypes.STRING
    },
    email: {
      index: { unique: true },
      type: DataTypes.STRING,
      validate: {
        isEmail:{
        isUnique(value, next) {
          Utilisateurs.find({
            where: { email: value }
          }).done((email) => {
            if (email)
              return next("Vous devez saisir un e-mail valide.");

            next();
          });
        }
        },
        notEmpty: {
          arg: true,
          msg: "Vous devez saisir un e-mail."
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          arg: true,
          msg: "Vous devez saisir un mot de passe."
        }
      }
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
    },
  }, {
    underscored: true,
    tableName: 'user',
  });

  Utilisateurs.beforeCreate((utilisateurs, options) => {
    return new Promise ((resolve, reject) => {
      bcrypt.hash(utilisateurs.password, (err, hash) => {
        utilisateurs.password = hash;
        return resolve(utilisateurs, options);
      });
    });
  });

  return Utilisateurs;
};
