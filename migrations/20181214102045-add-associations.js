'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn(
      'Utilisateurs', // name of Source model
      'restaurantId', // name of the key we're adding
      {
        type: Sequelize.INTEGER,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        references: {
          model: 'Restaurants',
          key: 'id'
        }
      }
    );
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.removeColumn(
      'Utilisateurs', 'restaurantId'
    );
  }
};
