'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn(
      'Produits', // name of Source model
      'restaurantId', // name of the key we're adding
      {
        type: Sequelize.INTEGER,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        references: {
          model: 'Restaurants',
          key: 'id'
        }
      }
    );
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.removeColumn(
      'Produits', 'restaurantId'
    );
  }
};
