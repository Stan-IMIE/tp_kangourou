'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn(
      'Restaurants', // name of Source model
      'categorieId', // name of the key we're adding
      {
        type: Sequelize.INTEGER,
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        references: {
          model: 'Categories',
          key: 'id'
        }
      }
    );
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.removeColumn(
      'Restaurants', 'categorieId'
    );
  }
};
